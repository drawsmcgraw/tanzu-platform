# Templated Product Configurations

These files are yaml files meant to be used by the `om` cli to upload to Ops Manager. 

NOTE: The pipeline expects a certain naming convention. Specifically, that the filename begin with its product slug, followed by `-config-templated.yml`. 

For example:

`elastic-runtime`-`config-templated.yml`.

Keep this in mind when building upon the pipeline.
