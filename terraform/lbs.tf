# Web Load Balancer

resource "aws_elb" "web" {
  name                             = "${var.environment_name}-web-lb"
  #load_balancer_type               = "network"
  #enable_cross_zone_load_balancing = true
  subnets                          = aws_subnet.public-subnet[*].id
  security_groups = [aws_security_group.web-lb.id]

  listener {
    instance_port     = 443
    instance_protocol = "tcp"
    lb_port           = 443
    lb_protocol       = "tcp"
  }

  listener {
    instance_port     = 80
    instance_protocol = "tcp"
    lb_port           = 80
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 6
    unhealthy_threshold = 3
    timeout             = 3
    target              = "HTTP:8080/health"
    interval            = 5
  }
}

#resource "aws_lb_listener" "web-80" {
#  load_balancer_arn = aws_lb.web.arn
#  port              = 80
#  protocol          = "TCP"
#
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.web-80.arn
#  }
#}
#
#resource "aws_lb_listener" "web-443" {
#  load_balancer_arn = aws_lb.web.arn
#  port              = 443
#  protocol          = "TCP"
#
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.web-443.arn
#  }
#}
#
#resource "aws_lb_target_group" "web-80" {
#  name     = "${var.environment_name}-web-tg-80"
#  port     = 80
#  protocol = "TCP"
#  vpc_id   = aws_vpc.vpc.id
#
#  health_check {
#    protocol = "TCP"
#  }
#}
#
#resource "aws_lb_target_group" "web-443" {
#  name     = "${var.environment_name}-web-tg-443"
#  port     = 443
#  protocol = "TCP"
#  vpc_id   = aws_vpc.vpc.id
#
#  health_check {
#    protocol = "TCP"
#  }
#}

# SSH Load Balancer

#resource "aws_lb" "ssh" {
#  name                             = "${var.environment_name}-ssh-lb"
#  load_balancer_type               = "network"
#  enable_cross_zone_load_balancing = true
#  subnets                          = aws_subnet.public-subnet[*].id
#}
#
#resource "aws_lb_listener" "ssh" {
#  load_balancer_arn = aws_lb.ssh.arn
#  port              = 2222
#  protocol          = "TCP"
#
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.ssh.arn
#  }
#}
#
#resource "aws_lb_target_group" "ssh" {
#  name     = "${var.environment_name}-ssh-tg"
#  port     = 2222
#  protocol = "TCP"
#  vpc_id   = aws_vpc.vpc.id
#
#  health_check {
#    protocol = "TCP"
#  }
#}
#
## TCP Load Balancer
#
#resource "aws_lb" "tcp" {
#  name                             = "${var.environment_name}-tcp-lb"
#  load_balancer_type               = "network"
#  enable_cross_zone_load_balancing = true
#  subnets                          = aws_subnet.public-subnet[*].id
#}
#
#locals {
#  tcp_port_count = 5
#}
#
#resource "aws_lb_listener" "tcp" {
#  load_balancer_arn = aws_lb.tcp.arn
#  port              = 1024 + count.index
#  protocol          = "TCP"
#
#  count = local.tcp_port_count
#
#  default_action {
#    type             = "forward"
#    target_group_arn = element(aws_lb_target_group.tcp[*].arn, count.index)
#  }
#}
#
#resource "aws_lb_target_group" "tcp" {
#  name     = "${var.environment_name}-tcp-tg-${1024 + count.index}"
#  port     = 1024 + count.index
#  protocol = "TCP"
#  vpc_id   = aws_vpc.vpc.id
#
#  count = local.tcp_port_count
#
#  health_check {
#    protocol = "TCP"
#  }
#}

# PKS API Load Balancer

resource "aws_lb" "pks-api" {
  name                             = "${var.environment_name}-pks-api"
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = true
  internal                         = false
  subnets                          = aws_subnet.public-subnet[*].id
}

resource "aws_lb_listener" "pks-api-9021" {
  load_balancer_arn = aws_lb.pks-api.arn
  port              = 9021
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pks-api-9021.arn
  }
}

resource "aws_lb_target_group" "pks-api-9021" {
  name     = "${var.environment_name}-pks-tg-9021"
  port     = 9021
  protocol = "TCP"
  vpc_id   = aws_vpc.vpc.id

  health_check {
    healthy_threshold   = 6
    unhealthy_threshold = 6
    interval            = 10
    protocol            = "TCP"
  }
}

resource "aws_lb_listener" "pks-api-8443" {
  load_balancer_arn = aws_lb.pks-api.arn
  port              = 8443
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pks-api-8443.arn
  }
}

resource "aws_lb_target_group" "pks-api-8443" {
  name     = "${var.environment_name}-pks-tg-8443"
  port     = 8443
  protocol = "TCP"
  vpc_id   = aws_vpc.vpc.id
}

###########
# Example #
###################################################################################################################
# When you create a workload k8s cluster, you need to expose the k8s master(s) on port 8443. In this instance,    #
# we create another NLB, just as we did for the PKS API server.                                                   #
# NOTE: This must be done for *each* k8s cluster deployed.                                                        #
# Uncomment and update the resources below to create an NLB for one k8s cluster.                                  #
###################################################################################################################
#
#resource "aws_lb" "k8s-dev-01" {
#  name                             = "${var.environment_name}-k8s-dev-01"
#  load_balancer_type               = "network"
#  enable_cross_zone_load_balancing = true
#  internal                         = false
#  subnets                          = aws_subnet.public-subnet[*].id
#}
#
#resource "aws_lb_listener" "k8s-dev-01-8443" {
#  load_balancer_arn = aws_lb.k8s-dev-01.arn
#  port              = 8443
#  protocol          = "TCP"
#
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.k8s-dev-01-8443.arn
#  }
#}
#
#resource "aws_lb_target_group" "k8s-dev-01-8443" {
#  name     = "${var.environment_name}-k8s-dev-01-tg-8443"
#  port     = 8443
#  protocol = "TCP"
#  vpc_id   = aws_vpc.vpc.id
#
#  health_check {
#    healthy_threshold   = 6
#    unhealthy_threshold = 6
#    interval            = 10
#    protocol            = "TCP"
#  }
#}

###########
# Example #
###############################################################################################################
# Manually creating an NLB for each k8s cluster becomes cumbersome. Using the below resources, we             #
# create an NLB (and the Listener and the Target Group) for every k8s cluster that appears in the             #
# variable 'k8s_clusters' in the file 'terraform.tfvars'.                                                        #
# To use the resources defined below, simply add the UUID for your k8s cluster to the 'k8s_uuids' variable    #
# and re-run Terraform.                                                                                       #
# NOTE: You still need to add the k8s master(s) to the Target Group yourself                                  #
###############################################################################################################
resource "aws_lb" "k8s-workload-cluster-nlb" {
  for_each = var.k8s_clusters
  name                             = "${var.environment_name}-${each.key}-k8s-nlb"
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = true
  internal                         = false
  subnets                          = aws_subnet.public-subnet[*].id
}

resource "aws_lb_listener" "k8s-workload-cluster-8443" {
  for_each = var.k8s_clusters
  load_balancer_arn = aws_lb.k8s-workload-cluster-nlb[each.key].arn
  port              = 8443
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.k8s-workload-cluster-8443[each.key].arn
  }
}

resource "aws_lb_target_group" "k8s-workload-cluster-8443" {
  for_each = var.k8s_clusters
  name     = "${var.environment_name}-${each.key}-k8s-tg"
  port     = 8443
  protocol = "TCP"
  vpc_id   = aws_vpc.vpc.id

  health_check {
    healthy_threshold   = 6
    unhealthy_threshold = 6
    interval            = 10
    protocol            = "TCP"
  }
}

