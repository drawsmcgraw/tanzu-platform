# Terraform Files

These files create a VPC, subnets, load balancers, DNS entries, and launch an OpsMan VM. You should use these only for reference or for a starting point for building your own project-specific Terraform files.

