environment_name = "ibcn"
hosted_zone = "ibcn.tacticalprogramming.com"

access_key = ""
secret_key = ""

region = "us-east-2"
availability_zones = ["us-east-2a", "us-east-2b"]

opsman_keypair = "20191007-aws-opsman"

# Version 2.5.19
#opsman_ami = "ami-0e95218643391c520"

# Version 2.9.5-build.144
opsman_ami = "ami-08a2f2acd85b8cdf5"

# This is used in `lbs.tf` to make load balancers for each k8s cluster.
k8s_clusters = [
# "dev-01"
]

# Example UUID Tags. In order for k8s masters to create ELBs on your behalf (when creating k8s services
# of type 'LoadBalancer', the public subnets must be tagged with the UUID of the respective k8s cluster.
# These values are added as tags via the file 'subnets.tf'.
# For more details, see https://docs.pivotal.io/pks/1-5/deploy-workloads.html#aws
# Uncomment and replace these entries with UUIDs that correspond to your k8s cluster (found by using the
# 'pks cluster <cluster-name>' command.
k8s_uuids = {
#   "kubernetes.io/cluster/service-instance_620d6212-c566-4caf-994c-5f1fcee326c3" = ""
#    "kubernetes.io/cluster/service-instance_2b33c2e6-0d68-4ca4-af48-0485b1cea3b7" = ""
#    "kubernetes.io/cluster/service-instance_35eb6349-725d-4c1b-a909-b5422fb9079e" = ""
#    "kubernetes.io/cluster/service-instance_48dd71c5-259a-4d57-a3bb-6d8bb7f660e7" = ""
#    "kubernetes.io/cluster/service-instance_96806ece-f42c-43aa-96dc-be0b962ae9ef" = ""
#    "kubernetes.io/cluster/service-instance_a6c6ff11-a063-451f-9bcb-5f6d41464f2c" = ""
#    "kubernetes.io/cluster/service-instance_ef10ba2b-a83a-4261-8d00-baf6c0f85f4b" = ""
}
