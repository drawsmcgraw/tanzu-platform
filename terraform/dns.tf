data "aws_route53_zone" "hosted" {
  name = var.hosted_zone
}

# Opsmanager for TAS
resource "aws_route53_record" "ops-manager" {
  name = "opsman.${data.aws_route53_zone.hosted.name}"

  zone_id = data.aws_route53_zone.hosted.zone_id
  type    = "A"
  ttl     = 300

  records = [aws_eip.ops-manager.public_ip]
}

resource "aws_route53_record" "wildcard-sys" {
  name = "*.sys.${data.aws_route53_zone.hosted.name}"

  zone_id = data.aws_route53_zone.hosted.zone_id
  type    = "A"

  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "wildcard-apps" {
  name = "*.apps.${data.aws_route53_zone.hosted.name}"

  zone_id = data.aws_route53_zone.hosted.zone_id
  type    = "A"

  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}

#resource "aws_route53_record" "ssh" {
#  name = "ssh.sys.${data.aws_route53_zone.hosted.name}"
#
#  zone_id = data.aws_route53_zone.hosted.zone_id
#  type    = "A"
#
#  alias {
#    name                   = aws_lb.ssh.dns_name
#    zone_id                = aws_lb.ssh.zone_id
#    evaluate_target_health = true
#  }
#}
#
#resource "aws_route53_record" "tcp" {
#  name = "tcp.${data.aws_route53_zone.hosted.name}"
#
#  zone_id = data.aws_route53_zone.hosted.zone_id
#  type    = "A"
#
#  alias {
#    name                   = aws_lb.tcp.dns_name
#    zone_id                = aws_lb.tcp.zone_id
#    evaluate_target_health = true
#  }
#}

resource "aws_route53_record" "pks-api" {
  name = "api.pks.${data.aws_route53_zone.hosted.name}"

  zone_id = data.aws_route53_zone.hosted.zone_id
  type    = "A"

  alias {
    name                   = aws_lb.pks-api.dns_name
    zone_id                = aws_lb.pks-api.zone_id
    evaluate_target_health = true
  }
}
