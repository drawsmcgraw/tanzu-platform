#!/bin/bash

# Download a product from an s3 blobstore, then upload it to OpsMan.
# Optionally also download/upload the stemcell.

fail_and_exit()
{
  echo "Previous command failed. Exiting."
  exit 1
}

# Are we also downloading the stemcell?
if [ ! -z "$DOWNLOAD_STEMCELL" ]; then
  STEMCELL_FLAG="--stemcell-iaas aws --download-stemcell=true --blobstore-stemcell-path $STEMCELLS_ROOT_DIR"
fi

mkdir $DOWNLOAD_DIR
om download-product $STEMCELL_FLAG \
  --output-directory $DOWNLOAD_DIR \
  --pivnet-product-slug $PRODUCT_SLUG \
  --file-glob *.pivotal \
  --product-version $PRODUCT_VERSION \
  --s3-endpoint $S3_ENDPOINT \
  --s3-access-key-id $AWS_S3_ACCESS_KEY \
  --s3-secret-access-key $AWS_S3_SECRET_KEY \
  --source s3 \
  --s3-region-name $S3_REGION \
  --blobstore-bucket $S3_BUCKET \
  --blobstore-product-path $PRODUCTS_ROOT_DIR/$PRODUCT_SLUG

if [ $? -ne 0 ]; then
  fail_and_exit
fi

# We're only downloading a single product at a time, so globbing *should* be safe.
om -k upload-product --product $DOWNLOAD_DIR/*.pivotal || fail_and_exit
om -k stage-product --product-name $STAGED_PRODUCT_NAME --product-version $PRODUCT_VERSION || fail_and_exit

# Upload stemcell if we downloaded it
# Again, only one stemcell in here, so we can glob it. The necessary square brackets in the
# filename make it a challenge due to shell interpolation.
if [ ! -z "$DOWNLOAD_STEMCELL" ]; then
  om -k upload-stemcell --floating=true --stemcell "$PWD/$DOWNLOAD_DIR"/*.tgz
fi
