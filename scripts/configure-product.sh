#!/bin/bash

envsubst < product_configs/${PRODUCT_SLUG}-config-templated.yml > ${PRODUCT_SLUG}-config.yml
om -k configure-product -c ${PRODUCT_SLUG}-config.yml
