# Tanzu Platform Automation
This repo is intended to automate the deployment of the Tanzu Application Service and supporting products.

# Overview
At a high level, the process is as follows:

* You pave your IaaS and deploy an OpsManager ([terraform](terraform) files are included if you'd like help)
* You stage all of your tiles and stemcells in an S3 Bucket
* You configure a copy of this repo with CI/CD variables
* You update the [gitlab-ci.yml](.gitlab-ci.yml) file to reflect your deployment environment
* You update the templates found in [product_configs](product_configs) to reflect your deployment environment
* You trigger this pipeline and it takes you from OpsMan to full TAS with tiles and TKGi deployed.

Lastly, you profit from having your infrastructure configuration saved in a versioning system.


# Getting Started
This repo will not work as-is. There is a fair amount of configuring that must be done, most of which involves creating variables in the CI/CD settings of your project. 

# Container Image
This pipeline assumes you're using a Gitlab Runner that uses containers. As such, you will need to create a container and store it in a registry that your Gitlab runner can access. Below is the Dockerfile used to build the container image used during testing of this pipeline.

```
FROM alpine:20200428
COPY om bin/.

# For 'envsubst'
RUN apk add gettext
```

You don't have to use this _exact_ Dockerfile, but you will need the packages/binaries installed. Specifically:
- `om` should be on the `$PATH`
- `envsubst` should be on the `$PATH`

# Terraform
If desired, the [terraform](terraform) directory has Terraform files for paving your IaaS.

## S3 Blobstore
This pipeline pulls artifacts from an S3 blobstore. As such, you will need a bucket dedicated to holding those files. Additionally, it expects a particular file/folder structure. See the example view below:

```
platform-artifacts/
├── products
│   ├── apm
│   │   └── [apm,1.6.1-build.74]apm-1.6.1-build.74.pivotal
│   ├── credhub-service-broker
│   │   └── [credhub-service-broker,1.4.7]credhub-service-broker-1.4.7.pivotal
│   ├── elastic-runtime
│   │   ├── [elastic-runtime,2.5.11]srt-2.5.11-build.2.pivotal
│   │   └── [elastic-runtime,2.8.9]srt-2.8.9-build.3.pivotal
│   ├── pivotal-container-service
│   │   └── [pivotal-container-service,1.6.1-build.6]pivotal-container-service-1.6.1-build.6.pivotal
│   └── p_spring-cloud-services
│       └── [p_spring-cloud-services,3.1.11]p_spring-cloud-services-3.1.11.pivotal
└── stemcells
    ├── [stemcells-ubuntu-xenial,170.217]light-bosh-stemcell-170.217-aws-xen-hvm-ubuntu-xenial-go_agent.tgz
    ├── [stemcells-ubuntu-xenial,456.100]light-bosh-stemcell-456.100-aws-xen-hvm-ubuntu-xenial-go_agent.tgz
    └── [stemcells-ubuntu-xenial,621.74]light-bosh-stemcell-621.74-aws-xen-hvm-ubuntu-xenial-go_agent.tgz
```

In the example above, we have a bucket named `platform-artifacts`. Under this bucket are two directories, `stemcells` and `products`. Stemcells contains, unsurprisingly, stemcells that will be uploaded to OpsMan. 

The directory `products` is further broken out by product. The name of each directory is important - they're named for the product slug. For example, the "Metrics" tile has a product slug of '`apm`'. When adding new products, you can find the slug by examining the URL from a search for that product on the Tanzu Network. To run with the "Metrics" example, the url https://network.pivotal.io/products/apm shows us that the "Metrics" tile has the product slug `apm`.

Additionally, the filenames are important. The `om` cli, which is used heavily in this pipeline, requires that the product version and slug be enclosed in square brackets at the start of the filename. Additionally, OpsMan has certain expectations about the rest of the filename. Long story short, don't deviate from the naming scheme above, which is:


## Configure Deployment Details
The [gitlab-ci.yml](.gitlab-ci.yml) contains a 'variables' section that you will need to modify to suit your deployment needs. We have provided sane defaults for a sample commercial AWS deployment as guidance.
## Configure Bosh Director
You will need to edit the [Bosh Director config file](director-config-templated.yml) to reflect your deployment. Specifically, you will need to address the following details:

* Networks (`networks-configuration` section), including CIDR blocks, DNS, and other details.
* Availability Zones (`az-configurations` section)
* IaaS configs (`iaas-configurations` section), including the Security Group. Note that this security group will also be associated with Kubernetes workload clusters created by TKGi

## Configure CI/CD Variables

### A note on formatting
Under the hood, all config files are yaml. This means indention matters. Additionally, due to technical challenges, some certificates and keys require inline newlines while others do not. Please see the relevant doc section below for guidance on formatting when applicable.

For variables that require in-line certs (i.e. include `\n` instead of newlines), you can open a normally-formatted x509 cert in `vim`, and then run the following command:

```:%s,\n,\\n,g```

That will replace all newlines with the literal text `\n`, which is the format you need.

The following variables need to be created in your project:

`AWS_S3_ACCESS_KEY`
`AWS_S3_SECRET_KEY`

These are the access/secret keypairs that the job uses when downloading files from your S3 Bucket, such as the TAS tile and other supporting products.

`S3_ENDPOINT`

This is the API endpoint for your S3 file storage. No bucket or filenames, just the api endpoint. For example, 
	

`OM_PASSWORD`
`OM_TARGET`
`OM_USERNAME`

These variables are used by the `om` cli when it interacts with Ops Manager.


`OPSMAN_BOSH_TRUSTED_CA_CHAIN`

The pem-encoded CA chain that will be installed on all Bosh-managed VMs. Formatting matters, _including spaces_.

Example:
```
      -----BEGIN CERTIFICATE-----
      MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/
      MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
      ...
```

`OPSMAN_DIR_SSH_PRIV_KEY_NAME`

The name of the SSH key as it appears in the AWS management console. For example, `tas-bosh-20200701`.


`OPSMAN_DIR_SSH_PRIV_KEY_CONTENTS`

The contents of the private key specified in the variable `OPSMAN_DIR_SSH_PRIV_KEY_NAME`. Formatting matters. 

Example:

```
-----BEGIN RSA PRIVATE KEY-----\r\nMIIEowIBAAKCAQEAzC2pDNmIf7zN8zaZNpo2ycUqQVP10cWJxNSWt8wMcm52bzSc\r\n9tSexample.....
```

`TAS_CREDHUB_KEY`

Credhub encryption key for TAS. Must be at least 20 characters long.

`TAS_OM_ENC_PASSPHRASE`

The decryption pass phrase used when accessing OpsMan after the VM boots. You will need to know this password every time you reboot the OpsMan VM.

`TAS_WILDCARD_CERT`

The pem-encoded wildcard cert used by the Application Service. Formatting matters.

Example:


```
-----BEGIN CERTIFICATE-----\nMIIF7jCCBNagAwIBAgISBBJ/dQUw2IeuuiJpnSZoq7o1MA0GCSqGSIb3DQEBCwUA\nMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD....
```

`TAS_WILDCARD_CERT_CA_CHAIN`

The pem-encoded CA chain that signed your wildcard cert. Formatting matters.

Example:

```
-----BEGIN CERTIFICATE-----\ngMIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\ngMSQwIgYDVQQKExtEaWdpdGFsIF...
```

`TAS_WILDCARD_KEY`

The pem-encoded key associated with your wildcard cert. Formatting matters.

Example:

```
-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCry0g5tE2/wCYD\nMiTOk1iF5lfqfJXKmVdv99skNtE6FKT/n/h7AIexample....
```

`TKGI_API_CERT`

The pem-encoded cert associated with your TKGi API host (the host you will target with the `pks` cli). Formatting matters.

Example:

```
-----BEGIN CERTIFICATE-----\nMIIEiTCCAnGgAwIBAgIRAICQi9spHBJPL4Jc6gslALEwDQYJKoZIhvcNAQELBQAw\nGTEXMBUGA1UEAxMOSGlw.....
```

`TKGI_API_KEY`

The pem-encoded key assocated with your TKGi API host (the host you will target with the `pks` cli). Formatting matters.

Example:

```
-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEA89nGR0ZBxGRcKqq9A8MsgnXWpMHw0nKFs0YJ1qHe6/KqjOH2\nDhJl04dGt5tn5sNOIx/6reannNcAKDFCexample...
```

`HARBOR_ADMIN_PASSWORD`

The admin password when logging into the Harbor UI.


`HARBOR_SMOKETEST_PASSWORD`

The 'smoketest' password found in the Opsman configuration.


`HARBOR_HOSTNAME`

The FQDN for the Harbor server. NOTE: this needs to match the CN and be entered as a SAN in the server cert.


`HARBOR_IP_ADDRESS`

The ip address to assign to Harbor. This must exist in the 'infra' network in Bosh and cannot be a 'reserved' IP.

`HARBOR_SERVER_CERT`

PEM-encoded x509 server cert for Harbor. This must include the `hostname` above as both the CN and as a SAN. Formatting matters, _including spaces_.

Example:
```
      -----BEGIN CERTIFICATE-----
      MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/
      MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
      ...
```


`HARBOR_SERVER_KEY`
PEM-encoded x509 server key for Harbor. Formatting matters, _including spaces_.

Example:
```
      -----exampleBEGIN RSA PRIVATE KEY-----
      MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/
      MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
      ...
```


`HARBOR_SERVER_CERT_CA`
PEM-encoded x509 server cert for the CA that signed the Harbor cert. This must include the `hostname` above as both the CN and as a SAN. Formatting matters, _including spaces_.

Example:
```
      -----BEGIN CERTIFICATE-----
      MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/
      MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
      ...
```


